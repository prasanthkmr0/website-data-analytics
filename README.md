# website-data-analytics



## Introduction

Preparing the website analytics from undefined format excel file (Improper) to csv file (Proper).

xlsx file contains website analytics with multiple fields and one month duration for each site.

csv file with specific fields for everyday analytics for each site. 

## Expected Input
xlsx file that contains website analytics
reference:
- [xlsx_file](https://gitlab.com/prasanthkmr0/website-data-analytics/-/blob/main/Analytics%20Template%20for%20Exercise.xlsx)

## Expected Output
csv file reference: 
- [csv_file](https://gitlab.com/prasanthkmr0/website-data-analytics/-/blob/main/output_report.csv)

## Installation

```
cd existing_repo
git clone https://gitlab.com/prasanthkmr0/website-data-analytics.git
cd website-data-analytics
pip install -r requirements.txt
```

## Instructions to run the project
```
python data_analytics.py
```

## Instructions to run tests
```
coverage run -m pytest
coverage report
coverage html
goto htmlcov folder and **data_analytics.py** in the brower
```

## Use cases and edge cases covered in the code
```
- The code contains to work on provided input only
- If you provide any other, it will print proper error and exit.
```

## Known Limitations
```
- input only xlsx file
- xlsx file should be like @Expected Input 
```
