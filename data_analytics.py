import pandas as pd
import numpy as np

class DataAnalytics:
    """Website data analytics
    """
    def __init__(self, file_name):
        """Constructor for intializing the required params
        """
        self.flag = True # Used for writing headers intially
        self.chunk_size = 3 # As we have each site details in 3 rows
        self.csv_file = open("output_report.csv", "w", newline="")
        self.file_name = file_name
        
    def file_validation(self):
        try:
            self.data = pd.read_excel(
                self.file_name,
                sheet_name = "input_refresh_template"
            )
            self.dates = pd.date_range(
                start = self.data.columns.values[0], 
                end = self.data.iloc[0, 0]
            )
            return True
        except (FileNotFoundError, AttributeError, ValueError, IndexError) as error:
            print(f"FILE_VALIDATION | ERROR: {error}")
            return False

    def chunk_data_process(self):
        """Data processing to generate the data in required format
        """
        if self.file_validation():
            try:
                for chunk in np.split(self.data, len(self.data) // self.chunk_size):
                    # Each chunk has 3 rows with all columns
                    for index in range(1, len(self.dates) + 1):
                        # index range from 1 to 31 based on A1 and A2
                        record = {}
                        date = self.dates[index - 1]
                        current_date = date.date()
                        record["Day of Month"] = current_date.day
                        record["Date"] = current_date
                        record["Site ID"] = chunk.iloc[2,0]
                        if chunk.iloc[0, index] == "Page Views" and str(chunk.iloc[1, index]) == str(date):
                            record["Page Views"] = chunk.iloc[2, index]
                        if chunk.iloc[0, index + len(self.dates)] == "Unique Visitors" and str(chunk.iloc[1, index + len(self.dates)]) == str(date):
                            record["Unique Visitors"] = chunk.iloc[2, index + len(self.dates)]
                        if chunk.iloc[0, index + (len(self.dates) * 2)] == "Total Time Spent" and str(chunk.iloc[1, index + (len(self.dates) * 2)]) == str(date):
                            record["Total Time Spent"] = chunk.iloc[2, index + (len(self.dates) * 2)]
                        if chunk.iloc[0, index + (len(self.dates) * 3)] == "Visits" and str(chunk.iloc[1, index + (len(self.dates) * 3)]) == str(date):
                            record["Visits"] = chunk.iloc[2, index + (len(self.dates) * 3)]
                        if chunk.iloc[0, index + (len(self.dates) * 4)] == "Average Time Spent on Site" and str(chunk.iloc[1, index + (len(self.dates) * 4)]) == str(date):
                            record["Average Time Spent on Site"] = chunk.iloc[2, index + (len(self.dates) * 4)]
                        if self.flag:
                            self.flag = False
                            pd.DataFrame([record]).to_csv(self.csv_file, header=True, index=False)
                        else:
                            pd.DataFrame([record]).to_csv(self.csv_file, header=None, index=False)
                return True
            except Exception as error:
                print(f"CHUNK_DATA_PROCESS | ERROR: {error}")
                print("Please try with Valid file")
        return False

if __name__ == "__main__":
    file_name = "Analytics Template for Exercise.xlsx"
    data_analytics = DataAnalytics(file_name)
    data_analytics.chunk_data_process()
