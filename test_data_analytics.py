import unittest
from data_analytics import DataAnalytics

class TestDataAnalytics(unittest.TestCase):
    def test_data_analytics_valid(self):
        file_name = "Analytics Template for Exercise.xlsx"
        data_analytics = DataAnalytics(file_name)
        response = data_analytics.chunk_data_process()
        self.assertTrue(response)

    def test_data_analytics_invalid(self):
        file_name = "file.xlsx"
        data_analytics = DataAnalytics(file_name)
        response = data_analytics.chunk_data_process()
        self.assertFalse(response)
